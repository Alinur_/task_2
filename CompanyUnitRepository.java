@Repository
public interface CompanyUnitRepository extends CrudRepository<CompanyUnit, Long> {
    @Query(value = "select * from company_unit", nativeQuery = true)
    CompanyUnit getCompanyUnit();

    List<CompanyUnit> findAllById(long c);

    List<CompanyUnit> findAll();
}