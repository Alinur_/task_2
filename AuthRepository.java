import java.util.List;
@Repository
public interface AuthRepository extends CrudRepository<Auth, Long>{
    @Query(value = "select * from users", nativeQuery = true)
    Auth getAuthBy();
    List<Auth> findAllByCompany_unit_id(long a);
    List<Auth> findAll();
}
