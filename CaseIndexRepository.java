@Repository

public interface CaseIndexRepository extends CrudRepository<CaseIndex, Long> {
    @Query(value = "select * from fond", nativeQuery = true)
    CaseIndex getCaseIndexBy();

    List<CaseIndex> findAllByCompany_unit_id(long c);

    List<CaseIndex> findAll();
}