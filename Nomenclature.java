@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="nomenclature")
public class Nomenclature {
    @Id
    private Long id;

    private String nomenclatureNumber;
    private Long year;
    @Column(name = "nomenclature_summary_id")
    private Long nomenclature_summary_id;

    @Column(name = "company_unit_id")
    private Long company_unit_id;

    private Long createdTimestamp;
    private Long createdBy;
    private Long updatedTimestamp;
    private Long updatedBy;
}