
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "nomenclature_summary")
public class NomenclatureSummary {
    @Id
    private Long id;

    private String number;
    private Long year;
    @Column(name = "company_unit_id")
    private Long company_unit_id;

    private Long createdTimestamp;
    private Long createdBy;
    private Long updatedTimestamp;
    private Long updatedBy;
}