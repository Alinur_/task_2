@Repository
public interface ActivateJournalRepository extends CrudRepository<ActivateJournal, Long> {
  @Query(value = "select * from users", nativeQuery = true)
   ActivateJournal getActivateJournalBy();

   List<ActivateJournal> findAllByObjectId(long a);

  List<ActivateJournal> findAll();
}