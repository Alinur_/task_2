@Repository
public interface TepfilesRepository extends CrudRepository<Tepfiles, Long> {
    @Query(value = "select * from users", nativeQuery = true)
    Tepfiles getTepfilesBy();
    List<Tepfiles> findAllById(long a);
    List<Tepfiles> findAll();
}