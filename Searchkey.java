@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "searchkey")
public class Searchkey {
    @Id
    private Long id;
    private String name;
    @Column(name = "company_unit_id")
    private Long company_unit_id;

    private Long createdTimestamp;
    private Long createdBy;
    private Long updatedTimestamp;
    private Long updatedBy;
}