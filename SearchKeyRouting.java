
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "search_key_routing")
public class SearchKeyRouting {
    @Id
    private Long id;

    @Column(name = "search_key_id")
    private Long search_key_id;
    private String tableName;

    private Long tableId;
    private String type;
}