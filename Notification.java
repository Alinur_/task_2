@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "notification")
public class Notification {
    @Id
    private Long id;

    private String objectType;
    private Long objectId;
    @Column(name = "company_unit_id")
    private Long company_unit_id;

    private Long userId;
    private Long createdTimestamp;
    private Long viewedTimestamp;
    private Long isViewed;
    private String title;
    private String text;
    private Long companyId;

}