@Repository
public interface LocationRepository extends CrudRepository<Location, Long> {
    @Query(value = "select * from users", nativeQuery = true)
    Location getLocationBy();
    List<Location> findAllByCompany_unit_id(long a);
    List<Location> findAll();
}