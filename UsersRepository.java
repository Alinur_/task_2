@Repository
public interface UsersRepository extends CrudRepository<Users, Long> {
    @Query(value = "select * from users", nativeQuery = true)
    Users getUsersBy();
    List<Users> findAllByAuthId(long u);
    List<Users> findAll();
}