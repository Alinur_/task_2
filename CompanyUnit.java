@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "company_unit")
public class CompanyUnit {
    @Id
    private Long id;

    private String nameRu;
    private String nameKz;
    private String nameEn;
    private Long parentId;
    private Long year;
    private Long companyId;
    private String codeIndex;
    private Long createdTimestamp;
    private Long createdBy;
    private Long updatedTimestamp;
    private Long updatedBy;
}