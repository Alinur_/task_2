@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="record")
public class Record {
    @Id
    private Long id;

    private String number;
    private String type;

    @Column(name = "company_unit_id")
    private Long company_unit_id;

    private Long createdTimestamp;
    private Long createdBy;
    private Long updatedTimestamp;
    private Long updatedBy;
}