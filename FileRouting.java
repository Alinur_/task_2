@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "file_routing")
public class FileRouting {
    @Id
    private Long id;

    private Long fileId;

    private String  tableName;
    private Long tableId;
    private String type;
}