@Repository
public interface CaseTableRepository extends CrudRepository<CaseTable, Long> {
    @Query(value = "select * from users", nativeQuery = true)
    CaseTable getCaseTableBy();
  List<CaseTable> findAllByLocationId(long a);
   List<CaseTable> findAll();
}