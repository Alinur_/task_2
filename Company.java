@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="company")
public class Company {
    @Id
    private Long id;

    private String nameRu;
    private String nameKz;
    private String nameEn;
    private String bin;
    private Long parentId;
    private Long fondId;
    private Long createdTimestamp;
    private Long createdBy;
    private Long updatedTimestamp;
    private Long updatedBy;
}