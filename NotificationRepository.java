@Repository
public interface NotificationRepository extends CrudRepository<Notification, Long> {
    @Query(value = "select * from users", nativeQuery = true)
    Notification getNotificationBy();
    List<Notification> findAllByObjectId(long a);
    List<Notification> findAll();
}