@Repository
public interface RequestRepository extends CrudRepository<Request, Long> {
    @Query(value = "select * from users", nativeQuery = true)
    Request getRequestBy();
    List<Request> findAllByRequest_user_id(long a);
    List<Request> findAll();
}