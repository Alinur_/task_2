@Repository
public interface DestructionActRepository extends CrudRepository<DestructionAct, Long> {
    @Query(value = "select * from users", nativeQuery = true)
   DestructionAct getDestructionActBy();
   List<DestructionAct> findAllByStructural_subdivision_id(long a);
   List<DestructionAct> findAll();
}