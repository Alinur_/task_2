@Repository
public interface FileRoutingRepository extends CrudRepository<FileRouting, Long> {
   @Query(value = "select * from users", nativeQuery = true)
  FileRouting getFileRoutingBy();

   List<FileRouting> findAllByFileId(long a);

    List<FileRouting> findAll();
}