@Repository
public interface SearchKeyRoutingRepository extends CrudRepository<SearchKeyRouting, Long> {
    @Query(value = "select * from users", nativeQuery = true)
    SearchKeyRouting getSearchKeyRoutingBy();
    List<SearchKeyRouting> findAllBySearch_key_id(long a);
    List<SearchKeyRouting> findAll();
}