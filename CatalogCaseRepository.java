@Repository
public interface CatalogCaseRepository extends CrudRepository<CatalogCase, Long> {
   @Query(value = "select * from users", nativeQuery = true)
    CatalogCase getCatalogCaseBy();
    List<CatalogCase> findAllByCaseId(long a);
    List<CatalogCase> findAll();
}