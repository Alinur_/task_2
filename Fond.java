@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "fond")
public class Fond {
    @Id
    private Long id;

    private String fondNumber;

    private Long createdTimestamp;
    private Long createdBy;
    private Long updatedTimestamp;
    private Long updatedBy;
}