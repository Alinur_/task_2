@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "share")
public class Share {
    @Id
    private Long id;

    private Long request_id;

    private String note;
    private Long senderId;
    private Long receiverId;
    private Long shareTimestamp;
}