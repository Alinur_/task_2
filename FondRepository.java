@Repository
public interface FondRepository extends CrudRepository<Fond, Long> {
    @Query(value = "select * from fond", nativeQuery = true)
    Fond getFond();
    List<Fond> findAllById(long a);
    List<Fond>  findAll();

}