@Repository
public interface CatologTableRepository extends CrudRepository<CatologTable, Long> {
    @Query(value = "select * from users", nativeQuery = true)
    CatologTable getCatologTableBy();
    List<CatologTable> findAllByCompany_unit_id(long a);
    List<CatologTable> findAll();
}