@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "catalog_case")
public class CatalogCase {
    @Id
    private Long id;
    private Long caseId;
    private Long catalogId;

    @Column(name = "company_unit_id")
    private Long company_unit_id;

    private Long createdTimestamp;
    private Long createdBy;
    private Long updatedTimestamp;
    private Long updatedBy;
}