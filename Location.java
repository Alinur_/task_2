@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Location")
public class Location {
    @Id
    private Long id;

    private String row;
    private String line;
    private String locationColumn;
    private String box;
    @Column(name = "company_unit_id")
    private Long company_unit_id;
    private Long createdTimestamp;
    private Long createdBy;
    private Long updatedTimestamp;
    private Long updatedBy;
}