@Repository
public interface ShareRepository extends CrudRepository<Share, Long> {
    @Query(value = "select * from users", nativeQuery = true)
    Share getShareBy();
    List<Share> findAllByRequest_id(long a);
    List<Share> findAll();
}