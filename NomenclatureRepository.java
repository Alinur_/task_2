@Repository
public interface NomenclatureRepository extends CrudRepository<Nomenclature, Long> {
    @Query(value = "select * from users", nativeQuery = true)
    Nomenclature getAuthBy();
    List<Nomenclature> findAllByCompany_unit_id(long a);
    List<Nomenclature> findAll();
}