@Repository
public interface FileRepository extends CrudRepository<File, Long> {
    @Query(value = "select * from users", nativeQuery = true)
    File getFileBy();
    List<File> findAllBySize(long a);
    List<File> findAll();
}