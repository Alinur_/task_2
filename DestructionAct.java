
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "destruction_act")
public class DestructionAct {
    @Id
    private Long id;

    private String actNumber;
    private String base;

    @Column(name = "structural_subdivision_id")
    private Long structural_subdivision_id;

    private Long createdTimestamp;
    private Long createdBy;
    private Long updatedTimestamp;
    private Long updatedBy;
}