@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tepfiles")
public class Tepfiles {
    @Id
    private Long id;

    private String fileBinary;
    @Column(name = "file_binary_byte")
    private String file_binary_byte;
}