@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "catolog_table")
public class CatologTable {
    @Id
    private Long id;

    private String nameRu;
    private String nameKz;
    private String nameEn;
    private Long parentId;
    @Column(name= "company_unit_id")
    private Long company_unit_id;

    private Long createdTimestamp;
    private Long createdBy;
    private Long updatedTimestamp;
    private Long updatedBy;

}