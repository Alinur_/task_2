import java.util.List;
@Repository
public interface SearchkeyRepository extends CrudRepository<Searchkey, Long> {
    @Query(value = "select * from users", nativeQuery = true)
    Searchkey getSearchKeyBy();
    List<Searchkey> findAllByCompany_unit_id(long a);
    List<Searchkey> findAll();
}