@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "status_request_history")
public class StatusRequestHistory {
    @Id
    private Long id;
    private Long requestId;

    private String status;

    private Long createdTimestamp;
    private Long createdBy;
    private Long updatedTimestamp;
    private Long updatedBy;
}