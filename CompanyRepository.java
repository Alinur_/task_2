@Repository
public interface CompanyRepository extends CrudRepository<Company, Long> {
    @Query(value = "select * from company", nativeQuery = true)
    Company getCompanies();

    List<Company> findAllById(long a);
    List<Company>  findAll();
}