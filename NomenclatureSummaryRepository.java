@Repository
public interface NomenclatureSummaryRepository extends CrudRepository<NomenclatureSummary, Long> {
    @Query(value = "select * from users", nativeQuery = true)
    NomenclatureSummary getNomenclatureSummaryBy();
    List<NomenclatureSummary> findAllByCompany_unit_id(long a);
    List<NomenclatureSummary> findAll();
}